import os
import sys
from bank import BankAccount
from gameplay import *


def menu():
    os.system('clear')
    print("WELCOME to the BlackJack!")

    print(" Options")
    print("[1] Play Game ")
    print("[2] Exit Game ")
    print("[3] List of Played Games ")
    option = input()
    return option


def main():
    option = menu()

    while True:
        os.system('clear')

        if "1" in option:
            # player game start
            game_play()
        elif "2" in option:
            print("Thanks for playing! Hope to see you again..")
            sys.exit()
        elif "3" in option:
            list_games()
            input("\n\nPress Enter to continue")
            main()

        option = menu()


main()
