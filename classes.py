from constants import *


class Deck:
    def __init__(self):
        self.deck = []

        for suit in SUITS:
            for number, value in NUMBERS.items():
                value = NUMBERS[number]
                self.deck.append({"suit": suit, "number": number, "value": value})


class Player:
    def __init__(self):
        self.card_set = []
        self.value = 0

    def sum_value(self, card_set):
        return sum(value['value'] for value in card_set)
