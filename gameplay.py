from classes import *
from utils import *
from readwritefile import *
from bank import BankAccount


def game_play():
    player_bet = 0
    new_deck = Deck()
    player_set_value = 0

    # game start
    person = Player()
    dealer = Player()

    print("Person starts with 100$")
    initial_deposit = 100
    account = BankAccount(0, initial_deposit)

    print("Starting cards for Player")
    for i in range(0, 2):
        person.card_set.append(take_card(new_deck.deck))
        check_ace_eleven_or_one(person)
        show_card(person.card_set[i]['number'], person.card_set[i]['suit'])

    person.value = person.sum_value(person.card_set)

    print("Starting cards for Dealer")
    for i in range(0, 2):
        dealer.card_set.append(take_card(new_deck.deck))
        check_ace_eleven_or_one(dealer)
        if i == 0:
            show_card(dealer.card_set[i]['number'], dealer.card_set[i]['suit'])
        elif i == 1:
            show_card("#", "#")

    dealer.value = dealer.sum_value(dealer.card_set)

    # game loop
    while True:
        endgame = False

        # player loop
        print("\n\nPlayer turn\n\n")
        while True:
            if check_player_win(person.value, account, player_bet):
                save(person, dealer)
                endgame = True
                break

            print("Options: H to hit , S to stop")
            option = input()
            if option == 'h':

                print("Want to bet? Balance ({})".format(account.value))
                value = int(input())
                account.withdraw(value)
                player_bet += value

                person.card_set.append(take_card(new_deck.deck))
                check_ace_eleven_or_one(person)
                show_player_cards(person.card_set)
                person.value = person.sum_value(person.card_set)

            elif option == 's':
                break
            else:
                print("\n\n\n\n\n Options: H to hit , S to stop")

        if endgame:
            break

        # dealer loop
        print("\n\nDealer turn\n\n")
        while True:
            if check_dealer_win(person.value, dealer.value, account, player_bet):
                save(person, dealer)
                endgame = True
                break

            dealer.card_set.append(take_card(new_deck.deck))
            check_ace_eleven_or_one(dealer)
            show_player_cards(dealer.card_set)
            dealer.value = dealer.sum_value(dealer.card_set)

        if endgame:
            break

    print("\n\nPerson have now in account: {}".format(account.value))
    input("Press Enter to restart")

