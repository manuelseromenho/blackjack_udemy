from random import randint


def show_card(number, suit):
    print("╒─────╕")
    print("|  {:3s}|".format(number))
    print("| {:3s} |".format(suit))
    print("╘─────╛")


def show_player_cards(card_set):
    for card in card_set:
        show_card(card['number'], card['suit'])
    print("-----..------")


def take_card(deck):
    card_taken = deck.pop(randint(0, len(deck) - 1))
    return card_taken


def check_player_win(player_value, account, player_bet):
    if player_value == 21:
        print("Player has won!")
        account.deposit(player_bet*2)
    elif player_value > 21:
        print("Player has lost!")
    else:
        return False
    return True


def check_dealer_win(player_value, dealer_value, account, player_bet):
    if dealer_value == 21:
        print("Dealer has won!")
    elif player_value < dealer_value <= 21:
        print("Dealer has won!")
    elif dealer_value > 21:
        print("Dealer has lost!")
        account.deposit(player_bet*2)
    else:
        return False
    return True


def check_ace_eleven_or_one(player):
    if player.sum_value(player.card_set) > 21:
        for card in player.card_set:
            if 'a' in card.values():
                    card['value'] = 1
