

def save_to_file(filename, person, dealer):
    f = open(filename, "a")
    f.write("\n Person Cards: ")
    for card in person.card_set:
        f.write(" {}[{}]".format(card['number'], card['suit']))
    f.write("\n Dealer Cards: ")
    for card in dealer.card_set:
        f.write(" {}[{}]".format(card['number'], card['suit']))
    f.close()


def save(person, dealer):
    filename = "demofile.txt"
    try:
        with open(filename) as file:
            save_to_file(filename, person, dealer)
    except IOError:
        open(filename, "x")
        save_to_file(filename, person, dealer)


def list_games():
    filename = "demofile.txt"
    f = open(filename, "r")
    print(f.read())
    f.close()
