#!/usr/bin/env python3
import signal
import os



# import os
# import sys
# import threading
# import time
# import signal
#
# def kill_handler():
#     data = sys.stdin.read(1)
#     os.kill(os.getpid(), signal.SIGINT)
#
# def main():
#     kill_thread = threading.Thread(target=kill_handler)
#     kill_thread.daemon = True
#     kill_thread.start()
#
#     print("press enter to terminate")
#     try:
#         while True:
#             print(1)
#             time.sleep(.5)
#             print(2)
#             time.sleep(.5)
#             print(3)
#             time.sleep(.5)
#             print(4)
#             time.sleep(.5)
#     except KeyboardInterrupt:
#         print("terminating")
#
# if __name__=='__main__':
#     main()


#.......................................#

# import keyboard
# import string
# from threading import *
#
#
# # I can't find a complete list of keyboard keys, so this will have to do:
# keys = list(string.ascii_lowercase)
# """
# Optional code(extra keys):
#
# keys.append("space_bar")
# keys.append("backspace")
# keys.append("shift")
# keys.append("esc")
# """
# def listen(key):
#     while True:
#         keyboard.wait(key)
#         print("[+] Pressed",key)
# threads = [Thread(target=listen, kwargs={"key":key}) for key in keys]
# for thread in threads:
#     thread.start()




# import keyboard  # using module keyboard
# keyboard.add_hotkey('ctrl+shift+a', print, args=('triggered', 'hotkey'))
# while True:
#     os.system('clear')
#     a = input("Insert a number: ")
#     b = input("Insert another number: ")
#     print("The sum is {}".format(int(a+b)))
#     input()







#....................................#


# from pynput import keyboard
#
# def on_press(key):
#     try:
#         print('alphanumeric key {0} pressed'.format(
#             key.char))
#     except AttributeError:
#         print('special key {0} pressed'.format(
#             key))
#
#
#
#
#
# while True:
#     with keyboard.Listener(on_press=on_press) as listener:
#         listener.join()
#     os.system('clear')
#     a = input("Insert a number: ")
#     b = input("Insert another number: ")
#     print("The sum is {}".format(int(a+b)))
#     input()





#....................................#


# def register_signal(signal_number, handler_func, once=False):
#     prev_handler = None
#
#     def _handler(signum, frame):
#         skip_prev = handler_func(signum, frame)
#
#         if not skip_prev:
#             if callable(prev_handler):
#                 if once:
#                     signal.signal(signum, prev_handler)
#                 prev_handler(signum, frame)
#             elif prev_handler == signal.SIG_DFL and once:
#                 signal.signal(signum, signal.SIG_DFL)
#                 os.kill(os.getpid(), signum)
#
#     prev_handler = signal.signal(signal_number, _handler)
#
#
# def keyboardHandler(signal, frame):
#     print(" (ID: {}) has been caught. Cleaning up...".format(signal))
#     exit(0)
#
# result = {'handler': 0}
#
# def _handler(signum, frame):
#     result['handler'] += 1
#
# register_signal(signal.SIGUSR1, _handler)
#
# os.kill(os.getpid(), signal.SIGUSR1)
# os.kill(os.getpid(), signal.SIGUSR1)
#
# signal.signal(signal.SIGUSR1, signal.SIG_DFL)
#
# # assertEqual(result['handler'], 2)




#....................................#


# def keyboardInterruptHandler(signal, frame):
#     print("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
#     exit(0)
#
# signal.signal(signal.SIGQUIT, keyboardInterruptHandler)
#
# while True:
#     os.system('clear')
#     a = input("Insert a number: ")
#     b = input("Insert another number: ")
#     print("The sum is {}".format(int(a+b)))
#     input()


#....................................#




# import termios, fcntl, sys, os
# fd = sys.stdin.fileno()
#
# oldterm = termios.tcgetattr(fd)
# newattr = termios.tcgetattr(fd)
# newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
# termios.tcsetattr(fd, termios.TCSANOW, newattr)
#
# oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
# fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
#
# try:
#     while 1:
#         try:
#             c = sys.stdin.read(1)
#             print("Got character", repr(c))
#             if "x1b" in repr(c):
#                 print("EXIT OPTION PRESSED")
#         except IOError: pass
# finally:
#     termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
#     fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)


# #!/usr/bin/python3
#
# # adapted from https://github.com/recantha/EduKit3-RC-Keyboard/blob/master/rc_keyboard.py
#
# import sys, termios, tty, os, time
#
# def getch():
#     fd = sys.stdin.fileno()
#     old_settings = termios.tcgetattr(fd)
#     try:
#         tty.setraw(sys.stdin.fileno())
#         ch = sys.stdin.read(1)
#     finally:
#         termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
#     return ch
#
# button_delay = 0.2
#
# while True:
#     char = getch()
#
#     if (char == "p"):
#         print("Stop!")
#         exit(0)