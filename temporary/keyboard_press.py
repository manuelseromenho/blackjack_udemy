#!/usr/bin/python3

# adapted from https://github.com/recantha/EduKit3-RC-Keyboard/blob/master/rc_keyboard.py
# see as well https://stackoverflow.com/questions/983354/how-do-i-make-python-to-wait-for-a-pressed-key
# see as well https://stackoverflow.com/questions/24072790/detect-key-press-in-python/24073884
# see as well https://pythonhosted.org/pynput/keyboard.html
# see as well arghh!!! https://stackoverflow.com/questions/25512319/python-terminating-your-program-at-any-time

import sys
import termios
import tty
import os
import time


def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)

    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

BUTTON_DELAY = 0.2
