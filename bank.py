import os


class BankAccount:
    def __init__(self, owner, value):
        self.owner = owner
        self.value = value

    def deposit(self, value):
        self.value = self.value + value
        print(f"The value of {value} has been added to the account. The total now is: {self.value}")

    def withdraw(self, value):
        if self.value - value < 0:
            print(f"There is not enough funds! Available: {self.value}")
        else:
            self.value = self.value - value
            print(f"The value of {value} has been withdraw. The total now is: {self.value}")

    def __str__(self):
        return f"The customer {self.owner} has the value of: {self.value}"

#
# def contains(list, filter):
# 	for x in list:
# 		if filter(x):
# 			return True
# 		return False


# list_accounts = []

# while True:
# 	os.system('cls' if os.name == 'nt' else 'clear')
#
# 	print("############# BANK MEGABRO ################")
# 	print("############THE BANK FOR YOU ##############")
#
# 	print("1) Create Account")
# 	print("2) Check Account")
# 	print("3) Account Name List")
# 	print("4) Deposit")
# 	print("5) Withdraw")
# 	print("0) Exit")
#
# 	option = int(input("Please select one option: "))
# 	if option == 0:
# 		print("The system is exiting now...")
# 		break
# 	elif option == 1:
# 		owner = input("Insert the name of the Owner: ")
# 		initial_deposit = int(input("Insert the initial deposit: "))
#
# 		list_accounts.append(BankAccount(owner, initial_deposit))
# 	elif option == 2:
# 		owner = input("Insert the name of the Owner: ")
#
# 		for i in list_accounts:
# 			if i.owner == owner:
# 				print(f"The customer {i.owner}, have a fund of {i.value}")
# 				input()
#
# 	elif option == 3:
# 		print("\n\n Account Name List \n \n")
# 		if list_accounts == []:
# 			print("\t\tNo accounts yet!")
# 		else:
# 			for account in list_accounts:
# 				print("Customer {} has {} euros.".format(account.owner, account.value))
# 		input()
# 	elif option == 4:
# 		owner = input("Insert the name of the Owner:")
#
# 		account = None
# 		for obj in list_accounts:
# 			if obj.owner == owner:
# 				account = obj
# 				print(obj)
# 				break
# 		if account:
# 			print(f"Please insert the value to deposit for the customer {owner}")
# 			value = int(input())
# 			account.deposit(value)
# 			input()
# 		else:
# 			print("no account found with this owner")
# 			input()
#
# 	elif option == 5:
# 		owner = input("Insert the name of the Owner:")
# 		account = None
#
# 		for obj in list_accounts:
# 			if obj.owner == owner:
# 				account = obj
# 				print(obj)
# 				break
# 		if account:
# 			print(f"Please insert the value to withdraw from the customer account of {owner}")
# 			value = int(input())
# 			account.withdraw(value)
# 			input()
# 		else:
# 			print("no account found with this owner")
# 			input()
